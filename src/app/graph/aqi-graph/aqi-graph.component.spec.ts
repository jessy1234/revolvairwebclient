import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AqiGraphComponent } from './aqi-graph.component';
import {ColorAqiEnum} from '../color-enum';

describe('AqiGraphComponent', () => {
  let component: AqiGraphComponent;
  let fixture: ComponentFixture<AqiGraphComponent>;
  let fakeData = [];

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AqiGraphComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AqiGraphComponent);
    component = fixture.componentInstance;

    for(let count = 5; count > 0; count--){
      fakeData[count] = count * 50;
    }
    component.data = fakeData;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe("ngAfterViewInit", () => {
    it('should set data in chart', () => {
      //arrange
      let expectedData = getExpectedData(fakeData);

      //act
      component.ngAfterViewInit();

      //assert
      expect(component.chart["config"]["data"]["datasets"][0]["data"]).toEqual(expectedData);
    });

    it('should set data in chart', () => {
      //arrange
      let expectedData = getExpectedData(fakeData);

      let expectedColors = [];
      for(let count = expectedData.length - 1; count >= 0; count--){
        if(expectedData[count] <= ColorAqiEnum.max_green)
          expectedColors[count] = ColorAqiEnum.green;
        if(expectedData[count] > ColorAqiEnum.max_green && expectedData[count] <= ColorAqiEnum.max_yellow)
          expectedColors[count] = ColorAqiEnum.yellow;
        if(expectedData[count] > ColorAqiEnum.max_yellow && expectedData[count] <= ColorAqiEnum.max_orange)
          expectedColors[count] = ColorAqiEnum.orange;
        if(expectedData[count] > ColorAqiEnum.max_orange && expectedData[count] <= ColorAqiEnum.max_red)
          expectedColors[count] = ColorAqiEnum.red;
        if(expectedData[count] > ColorAqiEnum.max_red && expectedData[count] <= ColorAqiEnum.max_purple)
          expectedColors[count] = ColorAqiEnum.purple;
        if(expectedData[count] > ColorAqiEnum.max_purple)
          expectedColors[count] = ColorAqiEnum.crimson;
      }

      //act
      component.ngAfterViewInit();

      //assert
      expect(component.chart["config"]["data"]["datasets"][0]["backgroundColor"]).toEqual(expectedColors);
    });

    it('should set labels in chart', () => {
      //arrange
      let expectedData = getExpectedData(fakeData);
      let labels = [];
      for(let count = fakeData.length - 2; count >= 0; count--){
        labels[fakeData.length - 2 - count] = count;
      }

      //act
      component.ngAfterViewInit();

      //assert
      expect(component.chart["config"]["data"]["labels"]).toEqual(labels);
    });
  })

});
function getExpectedData(fakeData){
  let expectedData = [];
  for(let count = 1; count <= fakeData.length -1 ; count++){
    expectedData[fakeData.length -1 - count] = count * 50;
  }
  return expectedData;
}
