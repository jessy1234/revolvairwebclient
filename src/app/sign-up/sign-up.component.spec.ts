import {async, ComponentFixture, fakeAsync, TestBed, tick} from '@angular/core/testing';

import { SignUpComponent } from './sign-up.component';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {Observable} from 'rxjs/Observable';
import {RegisterService} from '../services/register-service/register.service';
import {FormsModule} from '@angular/forms';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {EqualValidator} from '../validator/equal-validator.directive';
import {RouterModule} from '@angular/router';
import {RouterTestingModule} from '@angular/router/testing';

describe('SignUpComponent', () => {
  let component: SignUpComponent;
  let fixture: ComponentFixture<SignUpComponent>;
  let registerService: RegisterService;

  beforeEach(async(() => {

    TestBed.configureTestingModule({
    declarations: [
      SignUpComponent,
      EqualValidator
    ],
    providers: [
      RegisterService,
    ],
    imports: [
      HttpClientTestingModule,
      FormsModule,
      RouterTestingModule
    ],
    schemas: [NO_ERRORS_SCHEMA]
  }).compileComponents().then(() => {
    fixture = TestBed.createComponent(SignUpComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  registerService = TestBed.get(RegisterService);
  }));

afterEach(async(() => {
  component = null;
  fixture.destroy();
  registerService = null;
}));

  it('should create component', () => {
    expect(component).toBeTruthy();
  });

  it('should create emailService', () => {
    expect(registerService).toBeTruthy();
  });

  describe('onSubmit()', () => {
    it('should call registerService register method once', fakeAsync(() => {
      //Arrange
      spyOn(registerService, 'register').and.returnValue(Observable.of([]));
      spyOn((<any>component).router, 'navigate').and.returnValue(true);
      tick();

      //Act
      component.onSubmit();
      tick();

      //Assert
      expect(registerService.register).toHaveBeenCalledTimes(1);
    }));
  });

  describe('onSubmit()', () => {
    it('should go to sign-in page if response successful', fakeAsync(() => {
      //Arrange
      const expected = ['sign-in'];
      spyOn(registerService, 'register').and.returnValue(Observable.of([]));
      const spy = spyOn((<any>component).router, 'navigate').and.returnValue(true);
      tick();

      //Act
      component.onSubmit();
      tick();

      //Assert
      expect(spy).toHaveBeenCalledWith(expected);
    }));
  });
});
